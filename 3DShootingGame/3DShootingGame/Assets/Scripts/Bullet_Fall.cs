using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bullet_Fall : Bullet
{
    [SerializeField] private float riseTime;
    [SerializeField] private float fallTime;


    protected override void Move()
    {
        Vector3 targetRise = BulletShot._instance._risePos.transform.position;
        Vector3 aim = BulletShot._instance._aimPos.transform.position;
        var scale = transform.localScale;

        var sequence = DOTween.Sequence();
        sequence.Append(transform.DOMove(targetRise, riseTime));
        sequence.Join(transform.DOScale(Vector3.one * 2.0f, riseTime));
        sequence.Append(transform.DOMove(aim, fallTime));
        sequence.Join(transform.DOScale(Vector3.one / 2.0f, fallTime));


        sequence.Play()
            .SetLink(gameObject)
            .OnComplete(() =>
            {
                transform.localScale = scale;
                Destroy(gameObject);
            });
    }
}
