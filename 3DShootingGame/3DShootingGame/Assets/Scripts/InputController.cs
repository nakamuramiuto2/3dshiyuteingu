using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// プレイヤーのInputを管理する
/// </summary>
public class InputController : MonoBehaviour
{
    PlayerInput _input;
    Move _move;
    BulletShot _shot;
    Player _player;

    private void Awake()
    {
        TryGetComponent(out _input);
        TryGetComponent(out _move);
        TryGetComponent(out _shot);
        TryGetComponent(out _player);
    }

    private void OnEnable()
    {
        var nomal = _input.actions.FindActionMap("Nomal");
        nomal["Move"].performed += OnMove;
        nomal["Move"].canceled += OnMoveStop;
        nomal["Dash"].started += OnDash;
        nomal["Dash"].canceled += OnDashStop;
        nomal["ModeChange"].started += ToFallMode;

        var fall = _input.actions.FindActionMap("Fall");
        fall["Move"].performed += OnFallMove;
        fall["Move"].canceled += OnMoveStop;
        fall["Dash"].started += OnDash;
        fall["Dash"].canceled += OnDashStop;
        fall["ModeChange"].started += ToNomalMode;

        _input.actions["NomalShot"].started += OnNomalShot;
        _input.actions["SpecialShot"].started += OnSpecialShot;
        _input.actions["FallShot"].started += OnFallShot;
        _input.actions["Circle"].started += OnCircleShot;
    }

    private void OnDisable()
    {
        var nomal = _input.actions.FindActionMap("Nomal");
        nomal["Move"].performed -= OnMove;
        nomal["Move"].canceled -= OnMoveStop;
        nomal["Dash"].started -= OnDash;
        nomal["Dash"].canceled -= OnDashStop;
        nomal["ModeChange"].started -= ToFallMode;

        var fall = _input.actions.FindActionMap("Fall");
        fall["Move"].performed -= OnFallMove;
        fall["Move"].canceled -= OnMoveStop;
        fall["Dash"].started -= OnDash;
        fall["Dash"].canceled -= OnDashStop;
        fall["ModeChange"].started -= ToNomalMode;

        _input.actions["NomalShot"].started -= OnNomalShot;
        _input.actions["SpecialShot"].started -= OnSpecialShot;
        _input.actions["FallShot"].started -= OnFallShot;
        _input.actions["Circle"].started -= OnCircleShot;
    }

    /// <summary>
    /// 移動
    /// </summary>
    /// <param name="obj"></param>
    private void OnMove(InputAction.CallbackContext obj)
    {
        var axis = obj.ReadValue<Vector2>();
        var _velocity = new Vector3(axis.x, 0, axis.y);
        _move._velocity = _velocity;

        if (axis.y > 0)
            _move.RotateAirFrame("Left");
        else if (axis.y < 0)
            _move.RotateAirFrame("Right");
    }

    /// <summary>
    /// 投下モード　移動処理
    /// </summary>
    /// <param name="obj"></param>
    private void OnFallMove(InputAction.CallbackContext obj)
    {
        var axis = obj.ReadValue<Vector2>();
        var _velocity = new Vector3(axis.x / 2, 0, axis.y / 2);
        _move._velocity = _velocity;


        if (axis.y > 0)
            _move.RotateAirFrame("Left");
        else if (axis.y < 0)
            _move.RotateAirFrame("Right");
    }

    /// <summary>
    /// 移動が終わった時の処理
    /// </summary>
    /// <param name="obj"></param>
    private void OnMoveStop(InputAction.CallbackContext obj)
    {
        _move._velocity = Vector3.zero;
        _move.RotateRiset();
    }

    /// <summary>
    /// 通常弾を打つ処理
    /// </summary>
    /// <param name="obj"></param>
    private void OnNomalShot(InputAction.CallbackContext obj)
    {
        _shot.Shot();
    }

    /// <summary>
    /// スペシャル弾を打つ処理
    /// </summary>
    /// <param name="obj"></param>
    private void OnSpecialShot(InputAction.CallbackContext obj)
    {
        _shot.SpecialShot();
    }

    /// <summary>
    /// 投下弾を打つ処理
    /// </summary>
    /// <param name="obj"></param>
    private void OnFallShot(InputAction.CallbackContext obj)
    {
        _shot.FallShot();
    }    
    
    /// <summary>
    /// 円形攻撃　の処理
    /// </summary>
    /// <param name="obj"></param>
    private void OnCircleShot(InputAction.CallbackContext obj)
    {
        _shot.CircleShot();
    }

    /// <summary>
    /// 走る（スピードアップ）処理
    /// </summary>
    /// <param name="obj"></param>
    private void OnDash(InputAction.CallbackContext obj)
    {
        _move._speed *= 2.0f;
    }

    /// <summary>
    /// 走る　終了
    /// </summary>
    /// <param name="obj"></param>
    private void OnDashStop(InputAction.CallbackContext obj)
    {
        _move._speed = _move._speed / 2;
    }

    /// <summary>
    ///　通常モードに切り替え (ActionMap切り替え)
    /// </summary>
    /// <param name="obj"></param>
    private void ToNomalMode(InputAction.CallbackContext obj)
    {
        _input.SwitchCurrentActionMap("Nomal");
        _player.DisactiveText("Nomal");
    }

    /// <summary>
    /// 投下モードに切り替え (ActionMap切り替え)
    /// </summary>
    /// <param name="obj"></param>
    private void ToFallMode(InputAction.CallbackContext obj)
    {
        _input.SwitchCurrentActionMap("Fall");
        _player.DisactiveText("Fall");
    }
}
