using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using DG.Tweening;

public class TargetPoint : MonoBehaviour
{
    PlayerInput _input;
    Vector3 _velocity;
    [SerializeField] private float _speed;
    [SerializeField] private GameObject _player;

    private void Awake()
    {
        TryGetComponent(out _input);
        _velocity = Vector3.zero;
        transform.DOLocalRotate(new Vector3(0.0f, 0.0f, 360.0f), 3.0f)
            .SetRelative(true)
            .SetLoops(-1);
    }

    private void OnEnable()
    {
        _input.actions["Aim"].performed += OnAimMove;
        _input.actions["Aim"].canceled += OnAimMoveStop;

        transform.position = _player.transform.position;
    }

    private void OnDisable()
    {
        _input.actions["Aim"].performed -= OnAimMove;
        _input.actions["Aim"].canceled -= OnAimMoveStop;
    }

    private void OnAimMove(InputAction.CallbackContext obj)
    {
        var axis = obj.ReadValue<Vector2>();
        _velocity = new Vector3(axis.x, 0, axis.y);
    }
    private void OnAimMoveStop(InputAction.CallbackContext obj)
    {
        _velocity = Vector3.zero;
    }

    private void Update()
    {
        transform.position += _velocity * Time.deltaTime * _speed;
    }
}
