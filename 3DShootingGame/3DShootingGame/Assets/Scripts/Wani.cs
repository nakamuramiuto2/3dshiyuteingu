using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Wani : Enemy
{
    [SerializeField]
    private GameObject _enemyBullet;

    protected override void Move()
    {
        
    }
    protected override void Shot()
    {
        DOVirtual.DelayedCall(_fireInterval, () =>
         {
             for (int i = 0; i < _spawnPos.Length; i++)
             {
                 var spawn = _spawnPos[i].transform;
                 GameObject obj = Instantiate(_enemyBullet, spawn.position, spawn.rotation);
                 obj.transform.parent = _bulletBox.transform;
             }
         })
            .SetLoops(-1);
    }

}
