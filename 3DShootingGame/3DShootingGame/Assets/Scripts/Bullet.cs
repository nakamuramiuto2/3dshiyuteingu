using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    public float _speed;
    [SerializeField] protected Rigidbody _rb;
    [SerializeField] private float _lifeTime;

    void OnEnable()
    {
        TryGetComponent(out _rb);
        Destroy(gameObject, _lifeTime);
        Move();
    }

    protected abstract void Move();

    protected void OnCollisionEnter(Collision coll)
    {
        if(coll.gameObject.CompareTag("DisableArea"))
        {
            //ObjectPool.instance.ReleaseGameObject(gameObject);
            Destroy(gameObject);
        }

        if(gameObject.tag != "Enemy" && coll.gameObject.CompareTag("Enemy"))
        {
            //ObjectPool.instance.ReleaseGameObject(gameObject);
            Destroy(gameObject);
        }
    }
}