using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    public GameObject[] _movePos;
    public float[] _moveTime;
    public GameObject[] _spawnPos;
    public float _fireInterval;
    [SerializeField]
    protected GameObject _bulletBox;

    private void Awake()
    {
        Move();
        Shot();
    }

    protected abstract void Move();
    protected abstract void Shot();

    private void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.CompareTag("Bullet_Nomal") || coll.gameObject.CompareTag("Bullet_Special")
            || coll.gameObject.CompareTag("Bullet_Fall"))
        {
            Debug.Log("Hit Bullet");

            //ObjectPool.instance.ReleaseGameObject(coll.gameObject);
            Destroy(coll.gameObject);
        }
    }
}
