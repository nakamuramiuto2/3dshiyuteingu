using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum Type
{
    Right,
    Left
}

public class Bullet_Special : Bullet
{
    public Type _type;
    [SerializeField] private float _endTime;

    protected override void Move()
    {
        Vector3[] path = new Vector3[2];
        var pos = gameObject.transform.position;
        if(_type == Type.Right)
        {
            path[0] = new Vector3(pos.x + 4.0f, pos.y, pos.z + 4.0f);
            path[1] = new Vector3(pos.x, pos.y, pos.z + 15.0f);
        }
        else
        {
            path[0] = new Vector3(pos.x - 4.0f, pos.y, pos.z + 4.0f);
            path[1] = new Vector3(pos.x, pos.y, pos.z + 15.0f);
        }

        transform.DOLocalPath(path, _endTime, PathType.CatmullRom)
            .SetEase(Ease.Linear)
            .SetLink(gameObject)
            .OnComplete(CallbackFunction);
    }

    void CallbackFunction()
    {
        //ObjectPool.instance.ReleaseGameObject(gameObject);
        Destroy(gameObject);
    }
}
