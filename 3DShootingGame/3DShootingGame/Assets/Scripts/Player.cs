using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class Player : MonoBehaviour
{
    [SerializeField] private TextMeshPro _proUGUI;
    [SerializeField] private GameObject _targetObj;
    [SerializeField] private GameObject _changeText;

    void Start()
    {
        _targetObj.SetActive(false);
        _changeText.SetActive(false);
    }

    public void DisactiveText(string mode)
    {
        _proUGUI.SetText(mode);
        _changeText.SetActive(true);
        DOVirtual.DelayedCall(1.5f, () => _changeText.SetActive(false));

        if (mode == "Fall")
            _targetObj.SetActive(true);
        else
            _targetObj.SetActive(false);
    }

}
