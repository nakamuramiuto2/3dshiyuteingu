using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Nomal : Bullet
{
    protected override void Move()
    {
        _rb.velocity = transform.forward * _speed;
    }
}
