using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BulletShot : MonoBehaviour
{
    public static BulletShot _instance;

    [SerializeField] private GameObject _bullet;
    [SerializeField] private GameObject _spbull_right;
    [SerializeField] private GameObject _spbull_left;
    [SerializeField] private GameObject _fallBullet;

    [SerializeField] private GameObject _spawnPos;
    public GameObject _risePos;
    public GameObject _aimPos;
    public GameObject[] _specialBulletPos = new GameObject[3];

    bool _circleFlg = false;

    private void Awake()
    {
        _instance = this;
    }

    public void Shot()
    {
        //ObjectPool.instance.GetGameObject(_bullet, _spawnPos.transform.position, Quaternion.identity);
        var spawn = _spawnPos.transform;
        Instantiate(_bullet, spawn.position, spawn.rotation);
    }

    public void SpecialShot()
    {
        //ObjectPool.instance.GetGameObject(_spbull_right, _spawnPos.transform.position, Quaternion.identity);
        //ObjectPool.instance.GetGameObject(_spbull_left, _spawnPos.transform.position, Quaternion.identity);
        var spawn = _spawnPos.transform;
        Instantiate(_spbull_right, spawn.position, spawn.rotation);
        Instantiate(_spbull_left, spawn.position, spawn.rotation);
    }

    public void FallShot()
    {
        //ObjectPool.instance.GetGameObject(_fallBullet, _spawnPos.transform.position, Quaternion.identity);
        var spawn = _spawnPos.transform;
        Instantiate(_fallBullet, spawn.position, spawn.rotation);
    }

    public void CircleShot()
    {
        if (_circleFlg)
            return;
        else
        {
            _circleFlg = true;
            DOVirtual.DelayedCall(3.0f, () => _circleFlg = false);

            var insRotation = Quaternion.Euler(0, 0, 0);
            float angle = 0f;

            for (int i = 0; i < 36; i++)
            {
                Instantiate(_bullet, _spawnPos.transform.position, insRotation);
                angle += 10f;
                insRotation = Quaternion.Euler(0, angle, 0);
            }
        }

    }
}